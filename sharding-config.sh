#!/bin/bash
echo "Starting Mongo Sharding replica set configuration..."

#mongodb1=`getent hosts ${MONGO_SHA1} | awk '{ print $1 }'`
#mongodb2=`getent hosts ${MONGO_SHA2} | awk '{ print $1 }'`
#mongodb3=`getent hosts ${MONGO_SHA3} | awk '{ print $1 }'`

#port=${SHARDINGPORT:-27017}

#echo "Port is: ${port}"

#echo "Running mongo command: mongo --host ${MONGO_SHA1}:${PORT}"
# Running command on any single sharding server will add the rest to the given replica set
#mongo --host ${mongodb1}:${port} <<EOF

#docker exec -it mongo-shardSet0-1 bash -c "echo 'rs.initiate({_id : \"${SHARDING_REPLICA_SET}\", configsvr: false, version: 1, members: [{ _id : 0, host : \"${mongodb1}\" },{ _id : 1, host : \"${mongodb1}\" },{ _id : 2, host : \"${mongodb1}\" }]})' | mongo"


#docker exec -it mongo-shardSet0-1 bash -c "echo 'rs.initiate({_id : \"shardSet0\", configsvr: false, version: 1, members: [{ _id : 0, host : \"mongo-shardSet0-1\" },{ _id : 1, host : \"mongo-shardSet0-2\" },{ _id : 2, host : \"mongo-shardSet0-3\" }]})' | mongo"

#docker exec -it mongo-shardSet0-1 bash -c "echo 'rs.conf()' | mongo "

#docker exec -it mongo-shardSet0-1 bash -c "echo 'rs.status' | mongo "

echo "Initiating shardSet0 replica set..."
echo "Executing docker exec -i mongo-shardSet0-1 bash -c 'mongo --port 27017 --eval \"rs.initiate( { _id: \"shardSet0\", configsvr: false, version: 1, members: [{ _id: 0, host : \"mongo-shardSet0-1:27014\" }, { _id: 1, host : \"mongo-shardSet0-2:27015\" }, { _id: 2, host : \"mongo-shardSet0-3:27016\"}]})\"'"
docker exec -i mongo-shardSet0-1 bash -c 'mongo --port 27014 --eval "rs.initiate( { _id: \"shardSet0\", configsvr: false, version: 1, members: [{ _id: 0, host : \"mongo-shardSet0-1:27014\" }, { _id: 1, host : \"mongo-shardSet0-2:27015\" }, { _id: 2, host : \"mongo-shardSet0-3:27016\" }]})"'

docker exec -i mongo-shardSet1-1 bash -c 'mongo --port 27018 --eval "rs.initiate( { _id: \"shardSet1\", configsvr: false, version: 1, members: [{ _id: 0, host : \"mongo-shardSet1-1:27018\" }, { _id: 1, host : \"mongo-shardSet1-2:27019\" }, { _id: 2, host : \"mongo-shardSet1-3:27020\" }]})"'
echo "Mongo Sharding replication configuration done."
