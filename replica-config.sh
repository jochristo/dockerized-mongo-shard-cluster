#!/bin/bash
echo "Starting Mongo Replication configuration..."

#mongodb1=`getent hosts ${MONGO_CFG1} | awk '{ print $1 }'`
#mongodb2=`getent hosts ${MONGO_CFG2} | awk '{ print $1 }'`
#mongodb3=`getent hosts ${MONGO_CFG3} | awk '{ print $1 }'`

#port=${CONFIG_PORT:-27017}
#echo "Port is: ${port}"
#echo "CONFIG_PORT is: ${CONFIG_PORT}"

#docker exec -it mongo-config-server-1 bash -c "echo 'rs.initiate({_id : \"${CONFIG_REPLICA_SET}\", configsvr: true, version: 1, members: [{ _id : 0, host : \"${mongodb1}\" },{ _id : 1, host : \"${mongodb1}\" },{ _id : 2, host : \"${mongodb1}\" }]})' | mongo"

#docker exec -it mongo-config-server-1 bash -c "echo 'rs.conf()' | mongo "

#docker exec -it mongo-config-server-1 bash -c "echo 'rs.status' | mongo "


#docker exec -it mongo-config-server-1 bash -c "echo 'rs.initiate({_id : \"configSet0\", configsvr: true, version: 1, members: [{ _id : 0, host : \"mongo-config-server-1\" },{ _id : 1, host : \"mongo-config-server-2\" },{ _id : 2, host : \"mongo-config-server-3\" }]})' | mongo"

#docker exec -it mongo-config-server-1 bash -c "echo 'rs.conf()' | mongo "

#docker exec -it mongo-config-server-1 bash -c "echo 'rs.status' | mongo "

#echo "Running mongo command: mongo --host ${MONGO_CFG1}:${PORT}"
echo "Initiating configSet0 replica set..."
echo "Executing: rs.initiate( { _id: \"configSet0\", configsvr: true, version: 1, members: [{ _id: 0, host : \"mongo-config-server-1:27010\" }, { _id: 1, host : \"mongo-config-server-2:27011\" }, { _id: 2, host : \"mongo-config-server-3:27012\" }]})"
docker exec -i mongo-config-server-1 bash -c 'mongo --port 27010 --eval "rs.initiate( { _id: \"configSet0\", configsvr: true, version: 1, members: [{ _id: 0, host : \"mongo-config-server-1:27010\" }, { _id: 1, host : \"mongo-config-server-2:27011\" }, { _id: 2, host : \"mongo-config-server-3:27012\" }]})"'
# Running command on any single config server will add the rest to the given replica set
#rs.initiate( { _id: "configSet0", configsvr: true, version: 1, members: [{ _id: 0, host : "mongo-config-server-1:27010" }, { _id: 1, host : "mongo-config-server-2:27011" }, { _id: 2, host : "mongo-config-server-3:27012" }]})
#rs.conf()
#rs.status()

echo "Mongo Replication configuration done."
