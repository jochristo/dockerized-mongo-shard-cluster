#Configuration:
docker exec -i mongo-config-server-1 bash -c 'mongo --port 27010 --eval "rs.initiate( { _id: \"configSet0\", configsvr: true, version: 1, members: [{ _id: 0, host : \"mongo-config-server-1:27010\" }, { _id: 1, host : \"mongo-config-server-2:27011\" }, { _id: 2, host : \"mongo-config-server-3:27012\" }]})"'
#Shards:
docker exec -i mongo-shardSet0-1 bash -c 'mongo --port 27014 --eval "rs.initiate( { _id: \"shardSet0\", configsvr: false, version: 1, members: [{ _id: 0, host : \"mongo-shardSet0-1:27014\" }, { _id: 1, host : \"mongo-shardSet0-2:27015\" }, { _id: 2, host : \"mongo-shardSet0-3:27016\" }]})"'
docker exec -i mongo-shardSet1-1 bash -c 'mongo --port 27018 --eval "rs.initiate( { _id: \"shardSet1\", configsvr: false, version: 1, members: [{ _id: 0, host : \"mongo-shardSet1-1:27018\" }, { _id: 1, host : \"mongo-shardSet1-2:27019\" }, { _id: 2, host : \"mongo-shardSet1-3:27020\" }]})"'
#Router config:
docker exec -it mongo-router-1 bash -c 'mongo --port 27008 --eval "sh.addShard(\"shardSet0/mongo-shardSet0-1:27014\"); sh.addShard(\"shardSet1/mongo-shardSet1-1:27018\");"'
#OR
#docker exec -it mongo-router-2 mongo --port 27009