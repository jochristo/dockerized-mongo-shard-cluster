#Docker Mongos stop/rm containers:

docker stop mongo-router-1
docker rm mongo-router-1
docker stop mongo-router-2
docker rm mongo-router-2
docker stop mongo-shardSet0-1
docker stop mongo-shardSet0-2
docker stop mongo-shardSet0-3
docker stop mongo-shardSet1-1
docker stop mongo-shardSet1-2
docker stop mongo-shardSet1-3
docker rm mongo-shardSet0-1
docker rm mongo-shardSet0-2
docker rm mongo-shardSet0-3
docker rm mongo-shardSet1-1
docker rm mongo-shardSet1-2
docker rm mongo-shardSet1-3
docker stop mongo-config-server-1
docker stop mongo-config-server-2
docker stop mongo-config-server-3
docker rm mongo-config-server-1
docker rm mongo-config-server-2
docker rm mongo-config-server-3