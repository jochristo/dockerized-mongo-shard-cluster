#!/bin/bash
#echo "Running docker-compose-up"
#docker-compose up
#sleep 30
echo "Running bash script init.sh ..."
## Config servers setup
bash -c "sh ./replica-config.sh"
## Shard servers setup - works on any single server
bash -c "sh ./sharding-config.sh"
## Apply/Add sharding configuration with mongo router
bash -c "sh ./router-sharding.sh"
echo "Running init.sh script... Done."
